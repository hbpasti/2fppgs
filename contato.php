---
layout: none
---
<?php
header("Location: {{ site.baseurl }}#contato");
// Check for empty fields
if(empty($_POST['name'])  		||
   empty($_POST['email']) 		||
   empty($_POST['message'])	  ||
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
   {
    echo "No arguments Provided!";
    return false;
   }

$name = $_POST['name'];
$email_address = $_POST['email'];
$message = $_POST['message'];

// Create the email and send the message
$to = '{{ site.email }}'; 
$email_subject = "[2FPPGS] Mensagem de $name <$email_address> via site";
$email_body = "Mensagem de $name <$email_address> através do formulário de contato do site do 2FPPGS (www.ifch.unicamp.br/2fppgs).\n---\n\n$message";
// $headers = "From: $email_address\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
$headers = "From: {{ site.email }}\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
$headers .= "Reply-To: $email_address\nContent-Type: text/plain; charset=UTF-8";
mail($to,$email_subject,$email_body,$headers);
return true;
?>
